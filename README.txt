# Role email notifications version 1.0 dev #



This modules allows the configure of email notifications based on user roles. 

### How do I get this set up? ###

* Install and enable the module as normal
* Edit the role you would like to send a notification (/admin/people/permissions/roles/edit/3)
* Add the desired subject, message and the date for the notification

### Completed ###

1. Make each individual array as an object  
2. Using drupal date field on config page (Use verlidation of the field value rather than date field)
3. Add frequency(Every day / Every week / Every month / Every year)
4. Create a config page and have below two options avaiable (Roles / Individual Users)
5. Send email between 8AM to 6PM 



### Next version ###


* Token support
* Improve the instructions on the config form
* Log of emails sent?
* Specify the time to send emails


### Who do I talk to? ###

* Matt Shigang
